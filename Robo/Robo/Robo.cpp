#include "Robo.h"
#include "tga.h"
#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <GL/glut.h>
#ifdef _WIN32
#include <GL/glaux.h>
#endif

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

#define RAD(x)          (M_PI*(x)/180)
#define GRAUS(x)        (180*(x)/M_PI)

#define PASSO_TEMPO 5	// Passo do tempo
#define GRAU_RAD 0.017453292519943   //  Define a constante de convers�o de Graus para Radianos


//�ngulo posicao robo
float pf = 0;
//Tipo de camara 
int Acamara = 1;
//  Angulos de visualiza��o do observador
float teta = 0, distancia = 800.0;
// �ngulo cabe�a
float CR = 0;
int choosenArm = 0;
TGA  *chaoT,*mazeT,*starsT;

// �ngulos do ombro, cotovelo, m�o, p�, perna, perna1, joelho, tornozelo esquerdo
float OE = 0.0, OE1 = 0.0, CE = 0.0, CE1 = 0.0, ME = 0.0, PE = 0.0, PE1 = 0.0, JE = 0.0, TE = 0.0;
// �ngulos do ombro, cotovelo, m�o, p�, perna, perna1, joelho, tornozelo direito
float OD = 0.0,OD1=0, CD = 0.0, CD1 = 0.0, MD = 0.0, PD = 0.0, PD1 = 0.0, JD = 0.0, TD = 0.0;
#define MAZE_HEIGHT	 18
#define MAZE_WIDTH	 18
char mazedata[MAZE_HEIGHT][MAZE_WIDTH + 1] = {
	"                  ",
	" ******* ******** ",
	" *    *  *      * ",
	" * * *** * *    * ",
	" * **  * ** * * * ",
	" *     *  *   * * ",
	" *   **     *** * ",
	" *           *  * ",
	" *     * *** **** ",
	" * *   *   *    * ",
	" *   ****  *    * ",
	" ********  **** * ",
	" *            * * ",
	" *     *      * * ",
	" ** ** *    *** * ",
	" *   *      *   * ",
	" *******  **** ** ",
	"                  "
};
int maze[MAZE_HEIGHT][MAZE_WIDTH + 1];


//camara 3� pessoa
struct Estado
{
	double Px;
	double Py;
	double Pz = 125;
	double Cx = 12.5;
	double Cy = 15;
	double Cz = 125;
	double Upx = 0;
	double Upy = 0;
	double Upz = 1;

} camara;

//camara 1� pessoa
struct camara
{
	double Px = 7;
	double Py = 0;
	double Pz = 126;
	double Cx = Px + 100;
	double Cy = 7.5;
	double Cz = 126;
	double Upx = 0;
	double Upy = 0;
	double Upz = 1;

} camara1;

//camara up
struct camara2
{
	double Px = 12.5;
	double Py = 15;
	double Pz = 400;
	double Cx = 12.5;
	double Cy = 15;
	double Cz = 0;
	double Upx = 1;
	double Upy = 0;
	double Upz = 0;

} camara2;

struct posicaoRobo
{
	float x = 0;
	float y = 0;
	float direccao = 0;
	float velocidade = 1.0;
	float MovimentSpeed = 3.0;
}posRobo;


struct Teclas {
	GLboolean    w,s,d,a,g,h,z,j,l,n,m,k,i,c;
	GLboolean	 up, down, left, right;
}teclas;

void fixCam(void) {
	
	
		camara.Cx =  posRobo.x;
		camara.Cy =	posRobo.y;
		camara.Px = camara.Cx + distancia*cos(teta*GRAU_RAD);
		camara.Py = camara.Cy + distancia*sin(teta*GRAU_RAD);
	
		camara1.Px =  posRobo.x+(7 * cos((posRobo.direccao+CR)*GRAU_RAD));;
		camara1.Py =  posRobo.y+ (7 * sin((posRobo.direccao + CR)*GRAU_RAD));
		camara1.Cx = posRobo.x + (100*cos((posRobo.direccao + CR)*GRAU_RAD));
		camara1.Cy = posRobo.y + (100*sin((posRobo.direccao + CR)*GRAU_RAD));

		camara2.Cx =  posRobo.x;
		camara2.Cy =  posRobo.y;
		camara2.Px = camara2.Cx;
		camara2.Py = camara2.Cy;


	
}

// Cria Paralelep�pedos
void Paralelo(float comp, float lar, float alt , BOOLEAN texura)
{
	glBegin(GL_QUADS);
	glNormal3f(0, 0, -1); {
		if(texura)glTexCoord2d(0.0, 0.0);
		glVertex3f(0.0, 0.0, 0.0);
		if (texura)glTexCoord2d(1.0, 0.0);
		glVertex3f(lar, 0.0, 0.0);
		if (texura)glTexCoord2d(1.0, 1.0);
		glVertex3f(lar, comp, 0.0);
		if (texura)glTexCoord2d(0.0, 1.0);
		glVertex3f(0.0, comp, 0.0);
	}
	glNormal3f(0, 0, 1); {
		if (texura)glTexCoord2d(0.0, 0.0);
		glVertex3f(0.0, 0.0, alt);
		if (texura)glTexCoord2d(1.0, 0.0);
		glVertex3f(lar, 0.0, alt);
		if (texura)glTexCoord2d(1.0, 1.0);
		glVertex3f(lar, comp, alt);
		if (texura)glTexCoord2d(0.0, 1.0);
		glVertex3f(0.0, comp, alt);
	}
	glNormal3f(-1, 0, 0); {
		if (texura)glTexCoord2d(0.0, 0.0);
		glVertex3f(0.0, 0.0, 0.0);
		if (texura)glTexCoord2d(1.0, 0.0);
		glVertex3f(0.0, 0.0, alt);
		if (texura)glTexCoord2d(1.0, 1.0);
		glVertex3f(0.0, comp, alt);
		if (texura)glTexCoord2d(0.0, 1.0);
		glVertex3f(0.0, comp, 0.0);
	}
	glNormal3f(0, -1, 0); {
		if (texura)glTexCoord2d(0.0, 0.0);
		glVertex3f(0.0, 0.0, 0.0);
		if (texura)glTexCoord2d(1.0, 0.0);
		glVertex3f(0.0, 0.0, alt);
		if (texura)glTexCoord2d(1.0, 1.0);
		glVertex3f(lar, 0.0, alt);
		if (texura)glTexCoord2d(0.0, 1.0);
		glVertex3f(lar, 0.0, 0.0);
	}
	glNormal3f(1, 0, 0); {
		if (texura)glTexCoord2d(0.0, 0.0);
		glVertex3f(lar, 0.0, 0.0);
		if (texura)glTexCoord2d(1.0, 0.0);
		glVertex3f(lar, 0.0, alt);
		if (texura)glTexCoord2d(1.0, 1.0);
		glVertex3f(lar, comp, alt);
		if (texura)glTexCoord2d(0.0, 1.0);
		glVertex3f(lar, comp, 0.0);
	}
	glNormal3f(0, 1, 0); {
		if (texura)glTexCoord2d(0.0, 0.0);
		glVertex3f(0.0, comp, 0.0);
		if (texura)glTexCoord2d(1.0, 0.0);
		glVertex3f(0.0, comp, alt);
		if (texura)glTexCoord2d(1.0, 1.0);
		glVertex3f(lar, comp, alt);
		if (texura)glTexCoord2d(0.0, 1.0);
		glVertex3f(lar, comp, 0.0);
	}
	glEnd();
}
boolean checkPlace(float Nx , float Ny,int b)
{
	int i, j;
	boolean a = true;
	for (i = 0; i < MAZE_HEIGHT; i++) {
		for (j = 0; j < MAZE_WIDTH + 1; j++) {
			if (mazedata[i][j] == '*') {
				if (b == 1) {
					if (Nx - (50 * cos(posRobo.direccao*GRAU_RAD)) >= (i * 100) + 100 && Nx - (50 * cos(posRobo.direccao*GRAU_RAD)) <= (i * 100) + 200) {
						if (Ny - (50 * sin(posRobo.direccao*GRAU_RAD)) >= (j * 100) + 100 && Ny - (50 * sin(posRobo.direccao*GRAU_RAD)) <= (j * 100) + 200) {
							a = false;
						}
					}
				}
				if (b == 0) {
					if (Nx + (50 * cos(posRobo.direccao*GRAU_RAD)) >= (i * 100) + 100 && Nx + (50 * cos(posRobo.direccao*GRAU_RAD)) <= (i * 100) + 200) {
						if (Ny + (50 * sin(posRobo.direccao*GRAU_RAD)) >= (j * 100) + 100 && Ny + (50 * sin(posRobo.direccao*GRAU_RAD)) <= (j * 100) + 200) {
							a = false;
						}
					}

				}



			}
			
		}
	}
	if (Nx-50 < -5000 || Nx+50>5000 || Ny-50 < -5000 || Ny+50>5000)a = false;
	return a;
}
// Cria o piso
void Piso(void)
{
	int i, j, c = 100;
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, chaoT->getTextureHandle());
	//glColor3f(0.5f, 0.5f, 0.5f);
	for (i = -50; i <= 50; i++)
	{
		for (j = -50; j <= 50; j++)
		{




			glBegin(GL_QUADS);
			glNormal3f(0, 0, 1);
			glTexCoord2d(0.0, 0.0);
			glVertex3f(i * c, j * c, 0.0);
			glTexCoord2d(1.0, 0.0);
			glVertex3f(i * c + c, j * c, 0.0);
			glTexCoord2d(1.0, 1.0);
			glVertex3f(i * c + c, j * c + c, 0.0);
			glTexCoord2d(0.0, 1.0);
			glVertex3f(i * c, j * c + c, 0.0);
			glEnd();
		}
	}
	glBindTexture(GL_TEXTURE_2D, NULL);
	glDisable(GL_TEXTURE_2D);
	glEnable(GL_LIGHTING);
}

void desenhaLabirinto()
{
	
	int i, j;
	glColor3f(1.00, 1.49, 2.37);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, mazeT->getTextureHandle());
	glPushMatrix();
	glTranslatef(100, 100, 0);
	for (i = 0; i < MAZE_HEIGHT; i++) {
		for (j = 0; j < MAZE_WIDTH + 1; j++) {
			if (mazedata[i][j] == '*') {
				glPushMatrix();
				glTranslatef(i*100, j*100, 0);
				Paralelo(100, 100, 50,true);
				glPopMatrix();


			}
		}
	}

	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, NULL);
	glDisable(GL_TEXTURE_2D);
	glEnable(GL_LIGHTING);
}
void SkyBox()
{

	int i, j;
	glColor3f(1.00, 1.49, 2.37);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, starsT->getTextureHandle());
	glPushMatrix();
	glTranslatef(-5000, -5000, -10);
	Paralelo(10000,10000,8000, true);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, NULL);
	glDisable(GL_TEXTURE_2D);
	glEnable(GL_LIGHTING);
}
void createTextures()
{

	chaoT = new TGA("images/sand.tga");
	mazeT = new TGA("images/maze.tga");
	starsT = new TGA("images/stars.tga");


	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

}

// Cria o Rob�
void Robo(void)
{

	glPushMatrix(); {
		glTranslatef(posRobo.x, posRobo.y, 85);

		glRotatef(posRobo.direccao, 0, 0, 1);

		glTranslatef(-7.5, -12.5, 0.0);
		glPushMatrix(); {
			glColor3f(1.0, 1.0, 1.0);
			Paralelo(25.0, 15.0, 25.0,false); //     Tronco
			glColor3f(1.0, 1.0, 0.0);
			glTranslatef(7.0, 12.5, -6.0);
			glutSolidSphere(7.0, 20.0, 20.0);  //  Colo
			glPushMatrix(); {
				glColor3f(0.0, 0.0, 1.0);
				glTranslatef(0.0, 6, -8.0);
				glRotatef(PE, 0.0, 1.0, 0.0);
				glPushName(5);
				glutSolidSphere(4.0, 20.0, 20.0);  //  Articula��o da Perna Esquerda
				glColor3f(1.0, 0.0, 0.0);
				glTranslatef(-4.0, -4.0, -28.0);
				Paralelo(8.0, 8.0, 25.0, false);          //  Perna Esquerda
				glPushMatrix(); {
					glPopName();
					glPushName(6);
					glColor3f(0.0, 0.0, 1.0);
					glTranslatef(4.0, 4.0, -3.0);
					glRotatef(JE, 0.0, 1.0, 0.0);
					glutSolidSphere(4.0, 20.0, 20.0);  //  Joelho
					glColor3f(1.0, 0.0, 0.0);
					glTranslatef(-4.0, -4.0, -28.0);
					Paralelo(8.0, 8.0, 25.0, false);	//  Canela
					glColor3f(0.0, 0.0, 1.0);
					glTranslatef(4.0, 4.0, -3.0);
					glRotatef(TE, 0.0, 1.0, 0.0);
					glutSolidSphere(4.0, 20.0, 20.0);  //  Tornozelo
					glColor3f(1.0, 1.0, 0.0);
					glTranslatef(-6.0, -5.0, -8.0);
					Paralelo(10.0, 18.0, 5.0, false);	//  P�
					glPopName();
				}glPopMatrix();
			}glPopMatrix();
			glPushMatrix(); {
				glPushName(7);
				glColor3f(0.0, 0.0, 1.0);
				glTranslatef(0.0, -6.0, -8.0);
				glRotatef(PD, 0.0, 1.0, 0.0);
				glRotatef(PD1, 0.0, 0.0, 1.0);
				glutSolidSphere(4.0, 20.0, 20.0); //  Articula��o da Perna direita
				glColor3f(1.0, 0.0, 0.0);
				glTranslatef(-4.0, -4.0, -28.0);
				Paralelo(8.0, 8.0, 25.0, false);          //  Perna Direita
				glPopName();
				glPushMatrix(); {
					glPushName(8);
					glColor3f(0.0, 0.0, 1.0);
					glTranslatef(4.0, 4.0, -3.0);
					glRotatef(JD, 0.0, 1.0, 0.0);
					glutSolidSphere(4.0, 20.0, 20.0);  //  Joelho Direito
					glColor3f(1.0, 0.0, 0.0);
					glTranslatef(-4.0, -4.0, -28.0);
					Paralelo(8.0, 8.0, 25.0, false);		//  Canela
					glColor3f(0.0, 0.0, 1.0);
					glTranslatef(4.0, 4.0, -3.0);
					glRotatef(TD, 0.0, 1.0, 0.0);
					glutSolidSphere(4.0, 20.0, 20.0);  //  Tornozelo
					glColor3f(1.0, 0.0, 0.0);
					glTranslatef(-6.0, -5.0, -8.0);
					Paralelo(10.0, 18.0, 5.0, false);		//  P�
					glPopName();
				}glPopMatrix();
			}glPopMatrix();
			glPushMatrix(); {
				glPushName(1); {
					glColor3f(0.0, 0.0, 1.0);
					glTranslatef(0.0, -17.0, 27.0);
					glRotatef(OD, 0.0, 1.0, 0.0);
					glRotatef(OD1, 1.0, 0.0, 0.0);
					glutSolidSphere(5.0, 20.0, 20.0);    //Ombro Direito
					glColor3f(1.0, 0.0, 0.0);
					glTranslatef(-4.0, -4.0, -20.0);
					Paralelo(7.0, 7.0, 16, false);		//  Anti-bra�o
					
					}glPopName();
					glPushName(2); {
						glPushMatrix(); {
							glColor3f(0.0, 0.0, 1.0);
							glTranslatef(4.0, 3.0, -3.0);
							glRotatef(CD, 0.0, 1.0, 0.0);
							glRotatef(CD1, 0.0, 0.0, 1.0);
							glutSolidSphere(4.0, 20.0, 20.0);  //  Cotovelo
						glColor3f(1.0, 0.0, 0.0);
						glTranslatef(-4.0, -4.0, -19.0);
						Paralelo(8.0, 8.0, 16.0, false);		 //  Bra�o
						glColor3f(0.0, 0.0, 1.0);
						glTranslatef(4.0, 4.0, -2.0);
						glRotatef(MD, 0.0, 0.0, 1.0);
						glutSolidSphere(3.0, 20.0, 20.0);  //  Monheca
						glColor3f(1.0, 0.0, 0.0);
						glTranslatef(-4.0, -2.0, -12.0);
						Paralelo(4.0, 8.0, 10.0, false);		//  M�o
					}glPopName();
				}glPopMatrix();
			}glPopMatrix();
			glPushMatrix(); {
				glColor3f(0.0, 0.0, 1.0);
				glTranslatef(0.0, 17.0, 27.0);
				glRotatef(OE, 0.0, 1.0, 0.0);
				glRotatef(OE1, 1.0, 0.0, 0.0);
				glPushName(3); {
					glutSolidSphere(5.0, 20.0, 20.0);    //Ombro Esquerdo
					glColor3f(1.0, 0.0, 0.0);
					glTranslatef(-4.0, -3.0, -20.0);
					Paralelo(7.0, 7.0, 16.0, false);		//  Anti-bra�o
					
					}glPopName();
					glPushName(4); {
						glPushMatrix(); {
							glColor3f(0.0, 0.0, 1.0);
							glTranslatef(4.0, 4.0, -3.0);
							glRotatef(CE, 0.0, 1.0, 0.0);
							glRotatef(CE1, 0.0, 0.0, 1.0);
							glutSolidSphere(4.0, 20.0, 20.0);  //  Cotovelo
						glColor3f(1.0, 0.0, 0.0);
						glTranslatef(-4.0, -4.0, -19.0);
						Paralelo(8.0, 8.0, 16.0, false);		//  Bra�o
						glColor3f(0.0, 0.0, 1);
						glTranslatef(4.0, 4.0, -2.0);
						glRotatef(ME, 0.0, 0.0, 1.0);
						glutSolidSphere(3.0, 20.0, 20.0);  //  Monheca
						glColor3f(1.0, 0.0, 0.0);
						glTranslatef(-4.0, -2.0, -12.0);
						Paralelo(4.0, 8.0, 10.0, false);	  //  M�o
					}glPopName();
				}glPopMatrix();
			}glPopMatrix();
			glColor3f(0.0, 0.0, 1.0);
			glTranslatef(0.0, 0.0, 33.0);
			glutSolidSphere(4.0, 20.0, 20.0);    //Pesco�o
			glColor3f(1.0, 0.0, 0.0);
			glRotatef(CR, 0.0, 0.0, 1.0);
			glTranslatef(-6.0, -6.0, 3.0);
			Paralelo(12.0, 12.0, 12.0, false);//  Cabe�a
			glColor3f(1.0, 1.0, 1.0);
			glTranslatef(12, 4, 8);
			glutSolidSphere(1.0, 20.0, 20.0); //olho direita
			glTranslatef(0.7, 0, 0);
			glColor3f(0.0, 0.0, 0.0);
			glutSolidSphere(0.5, 20.0, 20.0);
			glTranslatef(0, 4, 0);
			glutSolidSphere(0.5, 20.0, 20.0);
			glColor3f(1.0, 1.0, 1.0);
			glTranslatef(-0.7, 0, 0);
			glutSolidSphere(1.0, 20.0, 20.0); // olho esquerda
			glColor3f(1.0, 1.0, 1.0);
			glTranslatef(-1, -6, -4);
			Paralelo(8, 2, 1.5, false);//boca

		}glPopMatrix();
	}glPopMatrix();
}

void initCamara(void) {
	switch (Acamara)
	{
	case 1:
		distancia = 800;
		teta = 0;
		break;
	case 3:
		camara2.Pz = 400;
		break;
	}
}
void setview(GLboolean Picking, int x, int y)
{
	int vport[4];


	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (Picking) { // se est� no modo picking, l� viewport e define zona de picking
		glGetIntegerv(GL_VIEWPORT, vport);
		gluPickMatrix(x, vport[3] - y, 4, 4, vport); // zona sobre o rato (+/-)
	}
	gluPerspective(24.0f, (GLfloat)glutGet(GLUT_WINDOW_WIDTH) / (GLfloat)glutGet(GLUT_WINDOW_HEIGHT), 0.5f, 10000.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

}
void setcam(void) {
	fixCam();
	switch (Acamara) {
	case 1:
		gluLookAt(camara.Px, camara.Py, camara.Pz, camara.Cx, camara.Cy, camara.Cz, camara.Upx, camara.Upy, camara.Upz);
		break;
	case 2:
		gluLookAt(camara1.Px, camara1.Py, camara1.Pz, camara1.Cx, camara1.Cy, camara1.Cz, camara1.Upx, camara1.Upy, camara1.Upz);
		break;
	case 3:
		gluLookAt(camara2.Px, camara2.Py, camara2.Pz, camara2.Cx, camara2.Cy, camara2.Cz, camara2.Upx, camara2.Upy, camara2.Upz);
		break;
	}

}
void Mouse(int bt, int st, int x, int y)
{
#define BUFF_SIZE		100
	int hits, i, k;
	GLuint   buffer[BUFF_SIZE], *bufp, numnames, name;
	GLdouble zmax, zmin;

	if (st != GLUT_DOWN)
		return;

	// inicio do picking

	glSelectBuffer(BUFF_SIZE, buffer);  // buffer onde coloca os dados do picking
	glRenderMode(GL_SELECT);            // inicializa o modo de picking
	glInitNames();                      // inicializa a pilha de nomes
	setview(GL_TRUE, x, y);            // define a vista para picking e a zona de picking
	glLoadIdentity();
	setcam();
	Robo();                             // desenha os objectos aos quais quer fazer picking
	hits = glRenderMode(GL_RENDER);     // inicializa o modo de rendering(devolve o numero de hits do picking)

										// an�lise dos resultados do picking

	if (hits)
	{
		k = 0;
		bufp = buffer;
		choosenArm = 0;
		for (i = 0; i<hits; i++)
		{
			// quantidade de nomes
			numnames = *bufp++;
			// profundidades da janela (serve para saber qual o objecto que est� � frente)
			zmin = (GLdouble)*bufp++ / UINT_MAX;  // cordenada z de janela minima do objecto entre 0 e 1
			zmax = (GLdouble)*bufp++ / UINT_MAX;  // cordenada z de janela maxima do objecto entre 0 e 1
			
			// nomes que o objecto tem
			while (numnames--) {
				name = *bufp++;
				choosenArm = name;

			}
		}
	}


	// redefinic�o da vista (pois foi alterada pelo picking)
	setview(GL_FALSE, x, y);
	glutPostRedisplay();
}


// Desenha na tela
void Desenhar(void)
{
	GLfloat posicao_luz[] = { 1000.0, 1000.0, 500.0, 1.0 }; // Posi��o Luz

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Posiciona a C�mera
	glLoadIdentity();
	setcam();

	// Habilita a luz	
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_POSITION, posicao_luz);
	Piso();	     // Desenha o Piso
	desenhaLabirinto();
	SkyBox();
	glPushMatrix();

	

	Robo();   // Desenha o Rob�

	glPopMatrix();

	glutSwapBuffers();
}

//   Fun��o para iluminar o ambiente
void Iluminacao(void)
{
	GLfloat ambiente[] = { 0.2, 0.2, 0.2, 1.0 };
	GLfloat difusa[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat especular[] = { 1.0, 1.0, 1.0, 1.0 };
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambiente);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, difusa);
	glLightfv(GL_LIGHT0, GL_SPECULAR, especular);
	glEnable(GL_LIGHT0);
	glEnable(GL_COLOR_MATERIAL);
}



//  Redimensiona a Janela
void Ajustedimensao(GLsizei w, GLsizei h)
{
	// N�o deixa a largura da janela ser menor que 700
	if (w < 700)	glutReshapeWindow(700, h);

	// N�o deixa a altura da janela ser menor que 700
	if (h < 500)	glutReshapeWindow(w, 500);

	glViewport(0, 0, w, h);

	setview(GL_FALSE, 0, 0);
}

void init_positionRobo() {
	
	CR = 0;

	OE = 0.0; OE1 = 0.0; CE = 0.0; CE1 = 0.0; ME = 0.0; PE = 0.0; PE1 = 0.0; JE = 0.0; TE = 0.0;
	OD = 0.0; OD1 = 0.0; CD = 0.0; CD1 = 0.0; MD = 0.0; PD = 0.0; PD1 = 0.0; JD = 0.0; TD = 0.0;

}



void teclado(unsigned char key, int a, int b)
{
	switch (key)
	{
	case 27:
		exit(0);
		break;

	case '1':
		Acamara = 1;
		break;
	case '2':
		Acamara = 2;
		break;
	case '3':
		Acamara = 3;
		break;

	case 'j':
		teclas.j = GL_TRUE;
		break;
	case 'l':
		teclas.l = GL_TRUE;
		break;
	case 'n':
		teclas.n = GL_TRUE;
		break;
	case 'm':
		teclas.m = GL_TRUE;
		break;
	case 'i':
		teclas.i = GL_TRUE;
		break;
	case 'k':
		teclas.k = GL_TRUE;
		break;
	case 'z':
		teclas.z = GL_TRUE;
		break;

	case 'x':
		init_positionRobo();
		break;	
	case 'w':
		teclas.w = GL_TRUE;
		break;
	case 's':
		teclas.s = GL_TRUE;
		break;
	case 'd':
		teclas.d = GL_TRUE;
		break;
	case 'g':
		teclas.g = GL_TRUE;
		break;
	case 'h':
		teclas.h = GL_TRUE;
		break;
	case 'a':
		teclas.a = GL_TRUE;
		break;
	case 'c':
		teclas.c = GL_TRUE;
		break;

	}
}

void Timer(int w)
{
	glutPostRedisplay();

	glutTimerFunc(PASSO_TEMPO, Timer, 1);

	static double passo = 0.0;

	//a��o nas teclas quando carregadas

	if (teclas.up) {

		PD = 20.0 * sin(passo * GRAU_RAD); // Movimenta perna direita
		OD = -30.0 * sin(passo * GRAU_RAD);	// Movimenta ombro direito
		PE = -20.0 * sin(passo * GRAU_RAD);	// Movimenta perna esquerda
		OE = 30.0 * sin(passo * GRAU_RAD);	// Movimenta ombro direito
		CD = -10.0;	// cotovelo direito
		CE = -10.0;	// cotovelo esquerdo

		JE = -20.0 * sin(passo * GRAU_RAD);	// Movimenta joelho esquerdo
		if (JE <= 0.0) JE = 0.0;

		JD = 20.0 * sin(passo * GRAU_RAD); // Movimenta joelho direito
		if (JD <= 0.0) JD = 0.0;
		
		passo = fmod(passo + posRobo.MovimentSpeed, 360);
		//posRobo.x += fabs(3.0 * sin((70 + passo) * GRAU_RAD));  //  Calcula a posi��o do Rob�
		float Nx = posRobo.x + (posRobo.velocidade * cos(posRobo.direccao*GRAU_RAD));
		float Ny = posRobo.y + (posRobo.velocidade * sin(posRobo.direccao*GRAU_RAD));
		if (checkPlace(Nx, Ny,0)) {
			posRobo.x = Nx;
			posRobo.y = Ny;
		}

	}


	if (teclas.down) {

		PD = 20.0 * sin(passo * GRAU_RAD); // Movimenta perna direita
		OD = -30.0 * sin(passo * GRAU_RAD);	// Movimenta ombro direito
		PE = -20.0 * sin(passo * GRAU_RAD);	// Movimenta perna esquerda
		OE = 30.0 * sin(passo * GRAU_RAD);	// Movimenta ombro direito
		CD = -10.0;	// cotovelo direito
		CE = -10.0;	// cotovelo esquerdo

		JE = -20.0 * sin(passo * GRAU_RAD);	// Movimenta joelho esquerdo
		if (JE <= 0.0) JE = 0.0;

		JD = 20.0 * sin(passo * GRAU_RAD); // Movimenta joelho direito
		if (JD <= 0.0) JD = 0.0;

		passo = fmod(passo + posRobo.MovimentSpeed, 360);
		//posRobo.x += fabs(3.0 * sin((70 + passo) * GRAU_RAD));  //  Calcula a posi��o do Rob�
	
		float Nx = posRobo.x - (posRobo.velocidade * cos(posRobo.direccao*GRAU_RAD));
		float Ny = posRobo.y - (posRobo.velocidade * sin(posRobo.direccao*GRAU_RAD));
		if (checkPlace(Nx, Ny,1)) {
			posRobo.x = Nx;
			posRobo.y = Ny;
		}

	}

	if (teclas.left) {
		if(teclas.down||teclas.up)
		posRobo.direccao += 1.0;

	}

	if (teclas.right) {
		if (teclas.down || teclas.up)
		posRobo.direccao -= 1.0;

	}
			
	if (teclas.j) {
		teta = teta - 1;
	}
	
	if (teclas.l) {
		teta = teta + 1;
	}
	
	if (teclas.k &&  camara.Pz>5) {
		camara.Pz = camara.Pz - 10;
	}

	if (teclas.i && camara.Pz<1000) {
		camara.Pz = camara.Pz + 10;
	}
	

	if (teclas.n) {
		if (Acamara == 1) { distancia = distancia + 10; }
		if (Acamara == 3) { camara2.Pz = camara2.Pz + 10; }
	}

	if (teclas.m) {
		if (Acamara == 1&& distancia>100) { distancia = distancia - 10; }
		if (Acamara == 3 && camara2.Pz>180) { camara2.Pz = camara2.Pz - 10; }
	}
			
	if (teclas.w)
	{
		if (choosenArm == 1 && OD>-90)OD = OD - 1;
		if (choosenArm == 2 && CD>-90)CD = CD - 1;
		if (choosenArm == 3 && OE>-90)OE = OE - 1;
		if (choosenArm == 4 && CE>-90)CE = CE - 1;
		if (choosenArm == 5 && PE > -90) PE = PE - 1;
		if (choosenArm == 6 && JE > 0) JE = JE - 1;
		if (choosenArm == 7 && PD > -90) PD = PD - 1;
		if (choosenArm == 8 && JD > 0) JD = JD - 1;

	}

	if (teclas.s)
	{
		if (choosenArm == 1 && OD<0)OD = OD + 1;
		if (choosenArm == 2 && CD<0)CD = CD + 1;
		if (choosenArm == 3 && OE<0)OE = OE + 1;
		if (choosenArm == 4 && CE<0)CE = CE + 1;
		if (choosenArm == 5 && PE < 60) PE = PE + 1;
		if (choosenArm == 6 && JE <160 ) JE = JE + 1;
		if (choosenArm == 7 && PD < 60) PD = PD + 1;
		if (choosenArm == 8 && JD <120) JD = JD + 1;
	}


	if (teclas.a)
	{
		if (choosenArm == 1 && OD1>-60)OD1 = OD1 - 1;
		if (choosenArm == 2 && CD1>-60)CD1 = CD1 - 1;
		if (choosenArm == 3 && OE1>0)OE1 = OE1 - 1;
		if (choosenArm == 4 && CE1>-60)CE1 = CE1 - 1;
	}


	if (teclas.d)
	{
		if (choosenArm == 1 && OD1<0)OD1 = OD1 + 1;
		if (choosenArm == 2 && CD1<60)CD1 = CD1 + 1;
		if (choosenArm == 3 && OE1<60)OE1 = OE1 + 1;
		if (choosenArm == 4 && CE1<60)CE1 = CE1 + 1;
	}

	
	if (teclas.h && CR>-90)
	{
		CR -= 1;
	}

	if (teclas.g && CR<90)
	{
		CR += 1;
	}

	if (teclas.z)
	{
		initCamara();
	}

	if (teclas.c) {
		posRobo.velocidade = 3;
		posRobo.MovimentSpeed = 7;
	}

	if (!teclas.c) {
		posRobo.velocidade = 1;
		posRobo.MovimentSpeed = 3;
	}

}

// Callback para interaccao via teclado (largar a tecla)

void KeyUp(unsigned char key, int x, int y)
{
	// ... accoes sobre largar teclas ...
	switch (key) {

	case 'j':
		teclas.j = GL_FALSE;
		break;
	case 'l':
		teclas.l = GL_FALSE;
		break;
	case 'k':
		teclas.k = GL_FALSE;
		break;
	case 'n':
		teclas.n = GL_FALSE;
		break;
	case 'm':
		teclas.m = GL_FALSE;
		break;
	case 'w':
		teclas.w = GL_FALSE;
		break;
	case 's':
		teclas.s = GL_FALSE;
		break;
	case 'd':
		teclas.d = GL_FALSE;
		break;
	case 'g':
		teclas.g = GL_FALSE;
		break;
	case 'h':
		teclas.h = GL_FALSE;
		break;
	case 'z':
		teclas.z = GL_FALSE;
		break;

	case 'i':
		teclas.i = GL_FALSE;
		break;
	case 'a':
		teclas.a = GL_FALSE;
		break;
	case 'c':
		teclas.c = GL_FALSE;
		break;

	}
}


// Callback para interaccao via teclas especiais  (carregar na tecla)

void SpecialKey(int key, int x, int y)
{

	switch (key) {
	case GLUT_KEY_RIGHT:
		teclas.right = GL_TRUE;
		break;
	case GLUT_KEY_LEFT:
		teclas.left = GL_TRUE;
		break;
	case GLUT_KEY_UP:
		teclas.up = GL_TRUE;
		break;
	case GLUT_KEY_DOWN:
		teclas.down = GL_TRUE;
		break;

	}

}

// Callback para interaccao via teclas especiais (largar na tecla)

void SpecialKeyUp(int key, int x, int y)
{
	switch (key) {
	case GLUT_KEY_RIGHT:
		teclas.right = GL_FALSE;
		break;
	case GLUT_KEY_LEFT:
		teclas.left = GL_FALSE;
		break;
	case GLUT_KEY_UP:
		teclas.up = GL_FALSE;
		break;
	case GLUT_KEY_DOWN:
		teclas.down = GL_FALSE;
		break;
	}

}


void imprime_ajuda(void)
{
  printf("\n\nCamara:\n");

  printf("1 - Camera 3 Pessoa\n");
  printf("2 - Camera 1 Pessoa\n");
  printf("3 - Camera vista de cima\n");
  printf("j - Girar Camera para a direita\n");
  printf("l - Girar Camera para a esquerda\n");
  printf("m - Aumentar zoom da Camera\n");
  printf("n - Diminuir zoom da Camera\n");
  printf("i - Subir Camera\n");
  printf("k - Descer Camera\n");
  printf("z - Inicializar posicao da camera\n");

  printf("\n\nRobo:\n");

  printf("Up,Down,Right,Left - Movimenta Robo\n");
  printf("c - Aumenta a velocidade\n");

  printf("w - Movimenta braco/antebraco/perna/joelho para cima\n");
  printf("s - Movimenta braco/antebraco/perna/joelho para baixo\n"); 
  printf("a - Movimenta braco/antebraco para direita\n");
  printf("d - Movimenta braco/antebraco para esquerda\n");

  printf("h - Gira a cabeca para a direita\n");
  printf("g - Gira a cabeca para a esquerda\n");
  printf("x - Inicializa a posicao dos membros do robo \n");

  printf("ESC - Sair\n");
}


// Programa Principal
int main(int argc, char*argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(150, 50);
	glutCreateWindow("Rob� em 3D");
	createTextures();
	glutDisplayFunc(Desenhar);
	glutReshapeFunc(Ajustedimensao);
	
	imprime_ajuda();

	//callbacks to teclado
	glutKeyboardFunc(teclado);
	glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(SpecialKey);
	glutSpecialUpFunc(SpecialKeyUp);
	glutMouseFunc(Mouse);

	Iluminacao();
	Timer(1);
	glutMainLoop();
}